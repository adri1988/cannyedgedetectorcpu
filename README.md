# README #

CPU approach of the canny edge detection algorithm in "C".


### How do I get set up? ###

* Any unix distribution
* clone or copy the code to a file with the .cu termination
* to compile "nvcc file.cu" -> ./a.out inputImage.bmp nameOfOutImage
					where input image MUST be .bmp file and nameOfOutputImage can be any name, this will create a .bmp file with this name.

### Who do I talk to? ###

* Repo owner or admin
